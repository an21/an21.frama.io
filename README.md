# L'an 21 — l'intégrale

![Un personnage triste dit: c'est de la fiction](an21_ex_libris_Maxim_Cain.jpg)

## D'où vient l'an 21 ?

L’An 21 est une série de 52 courtes utopies poétiques, humoristiques, absurdes parfois, qui ont pour but d’aider à appréhender le monde sous un angle différent. “Faire un pas de côté” comme disait Gébé, influence avouée. Elles ouvrent autant de pistes pour refaire le monde, que ce soit démocratiquement, sournoisement, connement, ou même accidentellement.

Ces 52 utopies étaient réparties en 12 livrets envoyés par la poste chaque mois de l’année 2021 à 250 heureux abonnés, environ. Chacun de ces livrets était illustré par un·e artiste de renom, de notre entourage, ou les deux.

Malgré cela, l'an 21 n'a pas changé le monde. Il en reste ces douze livrets, désormais laissés à disposition de tous.

## Qui ?
Écriture : Pierre Corbinais et Léo Duquesne.
Illustrations : Maxim Cain, Delphine Fourneau, Lucie Deroin, Benoît Preteseille, Anne Simon, Delphine Panique, Shyle Zalewski, Adrien Thiot-Rader, Adrien Houillère, Ariane Pinel, exaheva, tarmasz et Baladi.
Maquette : Joachim Werner
Relecture : Lucie Chausson, Pauline Duquesne & Julien Segura
Typographie : Infini, Sandrine Nugue, CNAP

## License
Cette œuvre est mise à disposition selon les termes de la [Licence Creative Commons Attribution - Pas d'Utilisation Commerciale - Pas de Modification 4.0 International](http://creativecommons.org/licenses/by-nc-nd/4.0/).
